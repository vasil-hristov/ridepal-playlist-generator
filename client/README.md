# RidePal Generator

### 1. Description

Welcome to our RidePal Generator! The front-end project is based on **React**. It is connected to the server of RidePal Generator. It uses the public REST service **Microsoft Bing** in order to calculate the distance and the estimated time for a journey from a **start point** to an **end point**. 

Our generator has a public and private part which will be revealed in details in point **5**. 

<br>

### 2. Project information

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **React**, **ESLint**, **Material-UI**, **React-Bootstrap**
- Public service REST API: **Microsoft Bing**

<br>

### 3. Setup

The `client` folder is designated as the **root** folder, where `package.json` is placed.

You need to install all the packages in the root folder: `npm i`.

- `npm start` - will run the current version of the code and will reflect any changes made in progress

<br>

### 4. Project structure

The API consists of several folders based on the REST principles: 

- `public` - it contains the **index.html** file which is visualised on the web page
The file **index.js** is on the root level and through it the page gets rerendered. 

- `src/App.js` - the entry point of the project
- `src/common` - this folder contains a file for constants
- `src/components` - this folder contains all the components structured by other folders
- `src/providers`- contains the files **AuthContext.js** and **GuardedRoute.js** which serve for authentication and authorization

<br>

### 5. Functionalities
Now is the moment to enter in details about the operations that can be executed. They can be reached by using several request verbs (**'get'**, **'post'**, **'put'**, **patch** and **'delete'**). 

The public part is accessible without the authentication. It supports the following functionalities: 

- register: http://localhost:3000/users (with **'post'** request)
- login: http://localhost:3000/users/login (with **'post'** request)
- logout: http://localhost:3000/users/logout (with **'post'** request)

The private part is only for registered users. It supports the following functionalities:

- view all playlists: http://localhost:3000/playlists (with **'get'** request)
- view a playlist by ID: http://localhost:3000/playlist/:id (with **'get'** request)
- create a playlist: http://localhost:3000/playlists/:id (with **'post'** request)
- update your own playlist: http://localhost:3000/playlists/:id (with **'put'** request)
- delete your own playlist: http://localhost:3000/playlists/:id (with **'delete'** request)

There can be users with a special role called admin. This role gives more power and the admin can exclusively do this:

- retrieve all users: http://localhost:5555/admin/users (with **'get'** request)
- **CRUD** (create, read, update, delete) operations for every user or admin
- The delete process is with soft delete and an admin can return an user back to active
- Admins also can change the roles to other admin or users


<br>

### Enjoy using our RidePal Generator. Feel free to contact us if you experience any bugs or problems. 

**Creators: Vasil Hristov, Aleksandar Dimitrov**
