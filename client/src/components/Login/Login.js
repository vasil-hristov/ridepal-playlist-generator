import jwtDecode from "jwt-decode";
import { useContext, useState } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import { API_BASE_URL } from "../../common/constants";
import AuthContext from "../../providers/AuthContext";
import PropTypes from "prop-types";
import './Login.css';
import { Button, TextField } from '@material-ui/core';

const Login = (props) => {
  const auth = useContext(AuthContext);
  const history = useHistory();
  const [error, setError] = useState(null);
  const [form, setForm] = useState({
    username: {
      placeholder: "Insert username",
      value: "",
      type: "text",
      valid: true,
      validate: (value) => value.length >= 3,
      errorMessage: "least 3 symbols long",
    },
    password: {
      placeholder: "Insert password",
      value: "",
      type: "password",
      valid: true,
      validate: (value) => value.length >= 6,
      errorMessage: "least 6 symbols long",
    },
  });

  const deleted = () => {
    localStorage.removeItem("token");
    alert("You are deleted! You do not have access!");
  };

  const login = (event) => {
    event.preventDefault();

    const isValid = Object.keys(form).every((key) =>
      form[key].validate(form[key].value)
    );

    if (isValid) {
      const data = Object.keys(form).reduce((acc, elementKey) => {
        return {
          ...acc,
          [elementKey]: form[elementKey].value,
        };
      }, {});

      fetch(`${API_BASE_URL}/users/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((r) => r.json())
        .then(({ token }) => {
          try {
            const user = jwtDecode(token);
            localStorage.setItem("token", token);
            auth.setAuthState({ user, isLoggedIn: true });
            if (user.is_deleted === 0) {
              history.push("/home");
              props.setUserMenu();
            } else {
              deleted();
            }
          } catch (error) {
            if (error.message === "Invalid token specified") {
              throw new Error("Invalid login data!");
            }
          }
        })
        .catch((error) => setError(error.message));
    }
  };
  const handleInputChange = (event) => {
    const { name, value } = event.target;

    form[name].value = value;
    form[name].valid = form[name].validate(value);
    form[name].touched = true;

    const updatedForm = { ...form };
    setForm(updatedForm);
  };

  const formElements = Object.keys(form)
    .map((key) => {
      return {
        id: key,
        config: form[key],
      };
    })
    .map(({ id, config }) => {
      return (
        <div key={id}>
            <TextField 
            id="standard-basic" 
            label={config.placeholder}
            helperText={config.valid ? null : config.errorMessage}
            type={config.type}
            name={id}
            placeholder={config.placeholder}
            value={config.value}
            onChange={handleInputChange}
          />
        </div>
      );
    });

  // if (error) {
  //   return (
  //     <AppError message={error} />
  //   );
  // }

  return (
    <>
      <div className="login">

        <div className="login-form mb-2">
          <h2>Login</h2>
          <form onSubmit={login}>
            {formElements}
            <br/>
            <Button  type="submit" variant="outlined"  size='small' color="primary">Log in</Button>{'   '}
            <Button size='small' variant="outlined" onClick={()=>history.push('/users')}>Sign up</Button>
            <br/>
            <br/>
          </form>
        </div>
      </div>
    </>
  );
};

Login.propTypes = {
  history: PropTypes.object.isRequired,
};

export default Login;
