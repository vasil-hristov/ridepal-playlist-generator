import React, { useContext, useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import { Link, useHistory } from 'react-router-dom';
import { API_BASE_URL, ROLES } from '../../common/constants';
import DEFAULT_COVER from '../../common/default-playlist-cover.jpg'
import './UpdatePlaylist.css';
import { DropzoneArea, DropzoneDialog } from 'material-ui-dropzone';
import InputLabel from '@material-ui/core/InputLabel';
import { TextField } from '@material-ui/core';
import AuthContext from '../../providers/AuthContext.js';

const UpdatePlaylist = ({ match }) => {

    const history = useHistory()
    const auth = useContext(AuthContext);
    const [open, setOpen] = useState(false);
    const id = match.params['id'];
    const [playlist, setPlaylist] = useState({ title: '' });
    const [error, setError] = useState(null);

    useEffect(() => {
        fetch(`${API_BASE_URL}/playlists/${id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then(e => e.json())
            .then(data => {
                if (auth.user.id !== data[0].users_id) {
                    if (auth.user.role !== ROLES.ADMIN) {
                        console.log(auth.user.id, data[0].users_id);
                        history.push('/home');
                    }
                }
                if (data[0].picture) {
                    getCover(data[0]);
                } else {
                    console.log(data[0]);
                    setPlaylist({
                        id: data[0].id,
                        users_id: data[0].users_id,
                        title: data[0].title,
                        cover: data[0].picture,
                    })
                }
            })
    }, []);

    const updatePlaylist = () => {
        fetch(`${API_BASE_URL}/playlists/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify(playlist)
        })
            .then(response => response.json())
            .then(result => {
                console.log(result);
                if (result.error) {
                    throw new Error(result.message);
                };

                history.goBack();
            })
            .catch(error => setError(error.message))
    };

    const updateTitle = (prop, value) => setPlaylist({
        ...playlist,
        [prop]: value
    });

    const getCover = (data) => {
        fetch(`${API_BASE_URL}/covers/${data.picture}`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                Accept: 'image/png',
            },
        })
            .then((response) => response.blob())
            .then((cover) => {
                setPlaylist({
                    id: data.id,
                    users_id: data.users_id,
                    title: data.title,
                    cover: data.picture,
                    coverblob: cover,
                });
            })
            .catch((error) => {
                console.warn(error);
            });
    };

    const setCover = (data) => {
        const file = playlist.coverblob;
        const formData = new FormData();
        formData.append('image', file);
        formData.append('id', playlist.id);

        fetch(`${API_BASE_URL}/playlists/covers`, {
            method: 'POST',
            body: formData,
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                Accept: 'multipart/form-data',
            },
        })
            .then((response) => response.json())
            .then((data) => {
                setPlaylist({ ...playlist, cover: data.cover });
            })
            .catch((error) => {
                console.warn(error);
            });
    };

    const deletePlaylist = (id) => {
        fetch(`${API_BASE_URL}/playlists/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .catch(console.log);
    }

    return (
        <div>
            <span className='cover-wrap'>
                {playlist.coverblob ? (
                    <img
                        className='cover'
                        src={playlist.coverblob ? URL.createObjectURL(playlist.coverblob) : null}
                        alt="err"
                    />
                )
                    :
                    <img
                        className='cover'
                        src={DEFAULT_COVER}
                        alt="err" />
                }
            </span>

            <span className='label'>
                <InputLabel display="inline">Insert new title:</InputLabel>
            </span>

            <span className='input'>
                <TextField id="standard-basic" value={playlist.title} onChange={e => updateTitle('title', e.target.value)} />
            </span>

            <span className='dropzone'>
                <Button variant="contained" size='small' color="primary" onClick={() => setOpen(true)}>
                    Change Cover
                    </Button>
                <DropzoneDialog
                    acceptedFiles={['image/*']}
                    cancelButtonText={"cancel"}
                    submitButtonText={"submit"}
                    maxFileSize={5000000}
                    open={open}
                    onClose={() => setOpen(false)}
                    onSave={(files) => {
                        setPlaylist({ ...playlist, coverblob: files[0] })
                        setOpen(false);
                    }}
                    showPreviewsInDropzone={true}
                    showPreviews={false}
                />
            </span>
            <span className='left-button'>
                {auth.user.role === ROLES.ADMIN || auth.user.id === playlist.users_id ?
                    <Button variant="contained" size='small' color="primary" onClick={() => { deletePlaylist(playlist.id); history.goBack() }}>
                        Delete
            </Button>
                    :
                    ''}
                <Button variant="contained" size='small' color="primary" onClick={() => { setCover(); updatePlaylist(); history.goBack() }}>
                    Save
            </Button>
                <Button variant="contained" size='small' color="primary" onClick={() => history.goBack()}>
                    Back
            </Button>
            </span>
        </div>
    )
};

export default UpdatePlaylist;
