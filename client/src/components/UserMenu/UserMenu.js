import { useContext, useEffect, useRef, useState } from 'react'
import { useHistory, withRouter } from 'react-router';
import { CSSTransition } from 'react-transition-group';
import { ROLES } from '../../common/constants.js';
import Login from '../Login/Login'
import './UserMenu.css'

const UserMenu = ({ authValue: auth, logout: logoutFun, ...props }) => {
  const history = useHistory();
  const [activeMenu, setActiveMenu] = useState(auth.isLoggedIn ? 'main' : 'login');
  const dropdownRef = useRef(null);


  window.addEventListener('click', e => e.pageX < window.innerWidth - 254 && props.setUserMenu(false))

  const logout = () => {
    console.log('logout');
    props.setUserMenu();
    logoutFun();
    history.push('/home');
  }

  const goTo = (page) => {
    console.log(page);
    props.setUserMenu();
    history.push(page);
  }

  const DropdownItem = (props) => (
    <span className="menu-item">
      <span className="icon-button">{props.icon}</span>
      {props.children}
    </span>
  );


  return (
    <div className="dropdown" ref={dropdownRef}>
      {activeMenu === 'main' ?
        <div
          classNames="menu-primary">
          <div className="menu">

            <DropdownItem icon={
              <img onClick={() => goTo('/updateUser')} src={props.avatar} alt='err' width='20px' height='20px' className='avatar' />
            }>
              <p onClick={() => goTo('/updateUser')} >{props.user_name || props.username || 'err'}</p>
            </DropdownItem>

            <DropdownItem>
              <p onClick={()=>goTo('/playlists')}>My playlists</p>
          </DropdownItem>

            <DropdownItem>
              <p onClick={()=>goTo('/playlists/create')}>Create playlist</p>
          </DropdownItem>

            {auth.user && auth.user.role === ROLES.ADMIN ?
              <DropdownItem>
                 <p onClick={()=>goTo('/admin/users')}>Admin Users</p>
          </DropdownItem> : <> </>}

            <DropdownItem>
              <p onClick={logout}>logout</p>
            </DropdownItem>

          </div>
        </div>
        :
        <div classNames="menu-secondary">
          <div className="login">
            <Login setUserMenu={props.setUserMenu} />
          </div>
        </div>
      }
    </div>
  );
}

export default withRouter(UserMenu);
