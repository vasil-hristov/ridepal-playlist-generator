import React, { useContext, useEffect, useState } from 'react';
import './Footer.css';

const Footer = () => {

  return (
    <div className="footer">
      RidePal 2021 ®️ All rights reserved
    </div>
  );
};

export default Footer;