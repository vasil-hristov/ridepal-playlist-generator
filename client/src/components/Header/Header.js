import React, { useContext, useEffect, useState } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';
import './Header.css';
import ExitToAppRoundedIcon from '@material-ui/icons/ExitToAppRounded';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import logo from '../../common/logo-ridepal.png';
import UserMenu from '../UserMenu/UserMenu.js';
import { API_BASE_URL } from '../../common/constants.js';
import blankAvatar from '../../common/index.png';


const Header = ({ authValue, logout }) => {
  const [userMenu, setUserMenu] = useState(false);
  const [user, setUser] = useState('login');
  const history = useHistory();
  const location = useLocation();
  useEffect(() => {
    if (authValue.isLoggedIn) {
      fetch(`${API_BASE_URL}/users`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          if (!data.avatar) {
            setUser({ ...authValue.user, avatar: blankAvatar });
          } else {
            getProfilePic(data);
          }
        })
        .catch(console.log);
    } else {
      setUser('login');
    }
  }, [location]);

  const getProfilePic = (user) => {
    fetch(`${API_BASE_URL}/avatars/${user.avatar}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        Accept: 'image/png',
      },
    })
      .then((response) => response.blob())
      .then((data) => {
        console.log(data);
        setUser({ ...user, avatar: URL.createObjectURL(data) });
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  return (
    <div className="nav-container">
      <div className="nav-bar-item nav-logo">
        <img src={logo} alt="logo" className="logo"  onClick={() => { history.push('/home') }}/>
      </div>
      <div className="nav-bar-item nav-menu">

        {authValue.isLoggedIn ? (
          <>
         
          {' '}
          <img
            src={user.avatar}
            alt='X'
            width="30px"
            height="30px"
            className="avatar"
            onClick={() => setUserMenu(!userMenu)}
          />
          {' '}
          </>
        )
        :
        <li className="nav-item">
          <ExitToAppRoundedIcon
            className="icon-button"
            onClick={() => setUserMenu(!userMenu)}/>
        </li>
        }
          {userMenu && (
            <UserMenu
              authValue={authValue}
              logout={logout}
              {...user}
              setUserMenu={() => setUserMenu(!userMenu)}
            />
          )}

      </div>
    </div>
  );
};

export default Header;
