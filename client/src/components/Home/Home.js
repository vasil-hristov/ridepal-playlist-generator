import { API_BASE_URL } from "../../common/constants.js";
import LOGO from '../../common/logo-ridepal.png'
import './Home.css';
import React, { useEffect, useState } from 'react';
import DEFAULT_COVER from '../../common/default-playlist-cover.jpg'
import { useHistory, useLocation } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import { getUser } from '../../providers/AuthContext.js';
import { Paper } from '@material-ui/core';
import { formatDuration } from '../../common/helpers.js';

const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    root: {
        flexGrow: 1,
    },
    paper: {
        height: 140,
        width: 100,
    },
    control: {
        padding: theme.spacing(2),
    },
}));

let stop=false;

const Home = () => {
    const classes = useStyles();
    const [playlists, setPlaylists] = useState([]);
    const history = useHistory();

    useEffect(() => {
            (async () => {
                await setPlaylists(await getPlaylists(5))
                
            })()
            stop=false;
    },[])

    useEffect(() => {
        console.log(stop);
        if(!stop){
        setTimeout(()=>{
            
            playlists.map((e, i) => {
                if (e.picture) {
                    getCover(e,i);
                }
            })
            stop=true;
        }
        , 1000)
    }
    }, [playlists])

    const getPlaylists = async (params, arrPlaylists = []) => {
        fetch(`${API_BASE_URL}/playlists/${params}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then(response => response.json())
            .then(result => {
                if (!result.message) {
                    arrPlaylists.push(result[0]);
                }
            })
            .catch(error => {
                console.log(error);
            })
        if (params > 1) {
            return await getPlaylists(params - 1, arrPlaylists);
        } else {
            console.log(arrPlaylists);
            return arrPlaylists;
        }
    };

    const getCover = (data, index) => {
        fetch(`${API_BASE_URL}/covers/${data.picture}`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                Accept: 'image/png',
            },
        })
            .then((response) => response.blob())
            .then((cover) => {
                console.log(playlists);
                playlists[index].coverblob = cover;
                setPlaylists([...playlists]);
            }
            )
            .catch((error) => {
                console.warn(error);
            });
    };

    return (
        <React.Fragment>
            <main>
                {/* Hero unit */}
                <div className={classes.heroContent}>
                    <Container maxWidth="sm">
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            <img src={LOGO} alt='RidePal' className='logo-home' />
                  RidePal
                </Typography>
                        <Typography variant="h5" align="center" color="textSecondary" paragraph>
                         Sound on! The playlist generator will make your travel better! 
                </Typography>
                        {getUser() ? <></> :
                            <div className={classes.heroButtons}>
                                <Grid container spacing={2} justify="center">
                                    <Grid item>
                                        <Button variant="contained" onClick={() => history.push('/users/login')} color="primary">
                                            Log in
                      </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button variant="outlined" onClick={() => history.push('/users')} color="primary">
                                            Sign up
                      </Button>
                                    </Grid>
                                </Grid>
                            </div>
                        }
                    </Container>
                </div>
                <Typography variant="h6" align="center" color="textSecondary" paragraph>
                    Here are some of the best playlists this month
                </Typography>
                <Container className={classes.root} maxWidth="md">
                    <div className='top-playlist'>
                    <Grid container justify="center" spacing={4}>
                        {playlists.map(value =>
                            <>
                                <Grid item>
                                    {console.log(value)}
                                    <Paper onClick={() => history.push('/playlists/' + value.id)} className={classes.paper}>
                                        {value.coverblob ?
                                            <img
                                                className='cover-card'
                                                src={URL.createObjectURL(value.coverblob)}
                                                alt="err"
                                            />
                                            :
                                            <img
                                                className='cover-card'
                                                src={DEFAULT_COVER}
                                                alt="err" />
                                        }
                                        <br />
                                        <span className='text-card'>{value.title}</span>
                                        <br />
                                        <span className='text-card'>{formatDuration(value.duration)}</span>
                                    </Paper>
                                </Grid>
                            </>)}
                    </Grid>
                    </div>
                </Container>
            </main>
        </React.Fragment>
    );
};

export default Home;
