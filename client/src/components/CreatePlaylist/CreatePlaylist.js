import React, { useState } from 'react';
import { API_BASE_URL } from '../../common/constants';
import PropTypes from 'prop-types';
import { GENRES } from '../../common/constants.js'
import { Link } from 'react-router-dom';
import DEFAULT_COVER from '../../common/default-playlist-cover.jpg'
import './CreatePlaylist.css'
import { Button, Checkbox, FormControlLabel, InputLabel, TextField } from '@material-ui/core';

const CreatePlaylist = ({ history }) => {
    const [locations, setLocations] = useState({
        start: null,
        end: null,
    })

    const [form, setForm] = useState({
        title: {
            placeholder: '',
            value: '',
            type: 'text',
            valid: true,
            validate: (value) => value.length >= 2,
            errorMessage: 'This field must be at least 2 symbols long'
        },
        duration: {
            placeholder: 'How long will you travel?',
            value: 0,
            type: 'number',
            valid: true,
            validate: (value) => typeof value === 'number' && value > 0 && value <= 15000,
            errorMessage: 'The value should be in the range of 0 and 7200 seconds',
        },
        genres: {
            placeholder: 'Insert genre',
            value: [],
            type: 'text',
            valid: true,
            validate: (value) => value,
            // errorMessage: 'This field must be at least 3 symbols long',
        },
        repeatArtist: {
            placeholder: 'Would you like more than one song by a single artist?',
            value: 0,
            type: 'checkbox',
            valid: true,
            validate: (value) => 1,
            errorMessage: 'Not recognized',
        },
    });


    const createPlaylist = async () => {
        await calcRouteDuration()
        const isValid = Object.keys(form)
            .every(key => form[key].validate(form[key].value));

        if (isValid) {
            const data = Object.keys(form)
                .reduce((acc, elementKey) => {
                    return {
                        ...acc,
                        [elementKey]: form[elementKey].value
                    }
                }, {});
            console.log(data);

            fetch(`${API_BASE_URL}/playlists`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                },
                body: JSON.stringify(data),
            }).then(response => response.json())
            .then(e=>e.id && history.push('/playlists/'+e.id+'/update'))
        }

    }
    const calcRouteDuration = async () => {

        try {
            return await fetch(`http://dev.virtualearth.net/REST/V1/Routes/Driving?wp.0=${locations.start}&wp.1=${locations.end}&avoid=minimizeTolls&key=AibL-mHgghKmW2HNyUldiX1iApN451Vc_nj5iub-Mtwx-GljtYtzt7zJzHCx_4ot`)
                .then(e => e.json())
                .then(e => e.resourceSets[0] && handleInputChange({ target: { value: e.resourceSets[0].resources[0].travelDuration, name: 'duration' } }));
        } catch {
            return null;
        }
    }

    const handleInputChange = event => {
        const { name, value, checked } = event.target;

        if (name === 'repeatArtist') {
            form[name].value = +checked;
        } else if (name === 'genres') {
            if (checked) {
                form[name].value.push(+event.target.id);
            } else {
                form[name].value.splice(form[name].value.indexOf(+event.target.id), 1);
            }
        } else {
            form[name].value = value;
            form[name].valid = form[name].validate(value);
        }
        form[name].touched = true;

        const updatedForm = { ...form };
        setForm(updatedForm);
    }

    const formElements = Object.keys(form)
        .map(key => {
            return {
                id: key,
                config: form[key]
            };
        })
        .map(({ id, config }) => {
            return (id === 'duration' ?
                <>
                    <br /><TextField id="standard-basic" label="Start"
                        value={locations.start}
                        type='text'
                        className='create-text-input'
                        onChange={(e) => setLocations({ ...locations, start: e.target.value })} />
                    <br />
                    <TextField id="standard-basic" label="End"
                        value={locations.end}
                        type='text'
                        className='create-text-input'
                        onChange={(e) => { setLocations({ ...locations, end: e.target.value }) }} />
                    <br />
                    <br />
                </>
                : id === 'genres' ?
                    <>
                        <div className="genres">
                            {Object.keys(GENRES).map((el) =>
                                <>
                                    <FormControlLabel
                                        value="start"
                                        control={
                                            <Checkbox
                                                color="primary"
                                                inputProps={{ 'aria-label': 'secondary checkbox' }}
                                                label={el}
                                                name="genres"
                                                type="checkbox"
                                                id={GENRES[el]}
                                                className='create-checkbox-input'
                                                onChange={handleInputChange} />}
                                        label={el}
                                        labelPlacement="start"
                                    />
                                    <br />
                                </>
                            )}
                        </div></>
                    : id === 'title' ?
                        <TextField id="standard-basic"
                            label="Title"
                            type={config.type}
                            name={id}
                            placeholder={config.placeholder}
                            value={config.value}
                            className='create-text-input'
                            onChange={handleInputChange} />
                        :
                        <div key={id}>
                            <FormControlLabel
                                value="start"
                                control={<Checkbox
                                    color="primary"
                                    inputProps={{ 'aria-label': 'secondary checkbox' }}
                                    style={config.valid ? null : ({ border: '1px solid red' })}
                                    type={config.type}
                                    name={id}
                                    placeholder={config.placeholder}
                                    value={config.value}
                                    className='create-checkbox-input'
                                    onChange={handleInputChange}
                                />}
                                label={"Repeat Artists"}
                                labelPlacement="start"
                            />
                            {!config.valid ? <span>{config.errorMessage}</span> : null}
                        </div>
            );
        })
    return (
        <>
            <div className="create-playlist">
                <span className='cover-create-wrap'>
                    <img
                        className='cover-create'
                        src={DEFAULT_COVER}
                        alt="err" />
                </span>
                <form onSubmit={createPlaylist}>
                    <h2>Create Playlist</h2>
                    {formElements}
                </form>
                <span className='create-btn'>
                    <Button type="submit" variant="contained" size='small' color="primary" onClick={createPlaylist}>Create</Button>
                </span>
            </div>
        </>
    )
};

CreatePlaylist.propTypes = {
    history: PropTypes.object.isRequired,
}

export default CreatePlaylist;
