import React, { useEffect, useState } from "react";
import { API_BASE_URL } from "../../common/constants.js";
import PropTypes from "prop-types";
import DEFAULT_COVER from '../../common/default-playlist-cover.jpg'
import "./GetPlaylistById.css";
import { formatDuration } from "../../common/helpers.js";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Card,
  CardContent,
  CardMedia,
  IconButton,
  InputLabel,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  withStyles,
} from "@material-ui/core";
import { Link } from "react-router-dom";

const GetPlaylistById = ({ match, history }) => {
  const [playlist, setPlaylist] = useState({});
  const [error, setError] = useState(null);
  const id = match.params["id"];
  const [tracks, setTracks] = useState("");

  useEffect(() => {
    fetch(`${API_BASE_URL}/playlists/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error(response.statusText);
      })
      .then((result) => {
        setPlaylist(result[0]);
        if (result[0].picture) {
        getCover(result[0]) 
        }else{
          loadPlaylistTracks(result[0]);
        }
      })
      .catch((e) => {
        setError(e.message);
      });
  }, [id]);

  useEffect(() => {
    if (tracks.length > 0) {
      loadArtist();
      loadAlbum();
    }
  }, [playlist]);

  const getCover = (data) => {
    fetch(`${API_BASE_URL}/covers/${data.picture}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        Accept: 'image/png',
      },
    })
      .then((response) => response.blob())
      .then((cover) => {
        
        setPlaylist({
          ...data,
          cover: data.picture,
          coverblob: cover,
        });
        loadPlaylistTracks({
          ...data,
          cover: data.picture,
          coverblob: cover,
        });
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const loadArtist = async () => {
    await Promise.all(
      tracks.map(
        async (t, index) =>
          await fetch(`${API_BASE_URL}/artists/${+t[0].artists_id}`, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          })
            .then((response) => {
              if (response.ok) {
                return response.json();
              }
              throw new Error(response.statusText);
            })
            .then((result) => {
              tracks[index].artist = result[0].name;
              setTracks([...tracks]);
            })
      )
    );
  };

  const loadAlbum = async () => {
    await Promise.all(
      tracks.map(
        async (a, index) =>
          await fetch(`${API_BASE_URL}/albums/${+a[0].albums_id}`, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          })
            .then((response) => {
              if (response.ok) {
                return response.json();
              }
              throw new Error(response.statusText);
            })
            .then((result) => {
              tracks[index].album = result[0].title;
              setTracks([...tracks]);
            })
      )
    );
  };

  const loadPlaylistTracks = async (playlist) => {
    const tracks = await Promise.all(
      playlist.tracks
        .split(" ")
        .slice(0, -1)
        .map(
          async (t) =>
            await fetch(`${API_BASE_URL}/tracks/${+t}`, {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
            }).then((result) => result.json())
        )
    );
    setTracks(tracks);
    setPlaylist({ ...playlist, tracksData: tracks });
  };

  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

  return (
    <>
    <div className='playlistById'>
      <span className='cover-id-wrap'>
        {playlist.coverblob ? (
          <img
            className='cover-id'
            src={URL.createObjectURL(playlist.coverblob)}
            alt="err"
          />
        )
          :
          <img
            className='cover-id'
            src={DEFAULT_COVER}
            alt="err" />
        }
      </span>
        <span className='label-1'>
          <InputLabel display="inline">Title:{playlist?.title}</InputLabel>
        </span>
        <span className='label-2'>
          <InputLabel display="inline">Duration:{formatDuration(playlist?.duration)}</InputLabel>
        </span>
        <span className='edit-btn'>
        <Button variant="contained" size='small' color="primary" onClick={() => history.push('/playlists/'+playlist.id+'/update')}>
          Edit
        </Button>
        </span>
    </div>
        <div className='tracks'>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className='accordion-heading'>View tracks</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <TableContainer className="table-tracks">
              <Table aria-label="customized table">
                <TableHead>
                  <TableRow>
                    <StyledTableCell>Play</StyledTableCell>
                    <StyledTableCell>Title</StyledTableCell>
                    <StyledTableCell>Duration</StyledTableCell>
                    <StyledTableCell>Artist</StyledTableCell>
                    <StyledTableCell>Album</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {playlist &&
                    playlist.tracksData &&
                    playlist.tracksData.map((t, index) => {
                      return (
                        <TableRow>
                          <StyledTableCell>
                            <a href={t[0].preview}>▶️</a>
                          </StyledTableCell>
                          <StyledTableCell>{t[0].title}</StyledTableCell>
                          <StyledTableCell>
                            {formatDuration(t[0].duration)}
                          </StyledTableCell>
                          <StyledTableCell>
                            {tracks ? tracks[index].artist : "loading"}
                          </StyledTableCell>
                          <StyledTableCell>
                            {tracks ? tracks[index].album : "loading"}
                          </StyledTableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
          </AccordionDetails>
        </Accordion>
        </div>
        </>
  );
};

GetPlaylistById.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default GetPlaylistById;
