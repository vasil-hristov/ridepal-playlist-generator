/* eslint-disable react-hooks/exhaustive-deps */
import { Button } from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { API_BASE_URL } from '../../common/constants.js';
import AuthContext from '../../providers/AuthContext.js';
import './GetPlaylists.css';
import DEFAULT_COVER from '../../common/default-playlist-cover.jpg'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { formatDuration } from '../../common/helpers.js'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        height: 140,
        width: 100,
    },
    control: {
        padding: theme.spacing(2),
    },
}));

const GetPlaylists = () => {
    const [playlists, setPlaylists] = useState([]);
    const auth = useContext(AuthContext);
    const history = useHistory();
    const [searchBy, setSearchBy] = useState({ "users_id": auth.user.id })
    const classes = useStyles();

    const logout = () => {
        localStorage.removeItem('token');
        auth.setAuthState({
            user: null,
            isLoggedIn: false,
        })
    }
    useEffect(() => {
        getPlaylists(searchBy);
    }, [searchBy]);

    useEffect(() => {
        if (playlists.length > 0) {
            playlists.map((e, i) => {
                if (e.picture && !e.coverblob) {
                    getCover(e, i);
                }
            });
        }
    }, [playlists])

    const getPlaylists = (searchParams) => {
        console.log(searchParams);
        fetch(`${API_BASE_URL}/playlists?${Object.keys(searchParams).map(e => e + '=' + searchParams[e])}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }

                if (response.status === 401) {
                    alert('Session expired!')
                    logout()
                }
                throw new Error(response.statusText)
            })
            .then(result => {
                if (Array.isArray(result)) {
                    console.log(result);
                    setPlaylists(result);
                } else {
                    throw new Error(result.message)
                }
            })
            .catch(error => {
                console.log(error);
            })
    };

    const getCover = (data, index) => {
        fetch(`${API_BASE_URL}/covers/${data.picture}`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                Accept: 'image/png',
            },
        })
            .then((response) => response.blob())
            .then((cover) => {
                console.log(playlists);
                playlists[index].coverblob = cover;
                setPlaylists([...playlists]);
            }
            )
            .catch((error) => {
                console.warn(error);
            });
    };

    return (
        <div className="all-playlists">
            <span className='all-btn'>
                <Button variant="contained" size='small' color="primary" onClick={() => getPlaylists({})}>show all</Button>
            </span>
            <Grid container className={classes.root} spacing={2}>
                <Grid item xs={12}>
                    <Grid container justify="center" spacing={2}>
                        {Array.isArray(playlists) && playlists.map((value) => (
                            <Grid key={value.title} item>
                                <Paper onClick={() => history.push('/playlists/' + value.id)} className={classes.paper}>
                                    {value.coverblob ?
                                        <img
                                            className='cover-card'
                                            src={URL.createObjectURL(value.coverblob)}
                                            alt="err"
                                        />
                                        :
                                        <img
                                            className='cover-card'
                                            src={DEFAULT_COVER}
                                            alt="err" />
                                    }
                                    <br />
                                    <span className='text-card'>{value.title}</span>
                                    <br />
                                    <span className='text-card'>{formatDuration(value.duration)}</span>
                                </Paper>
                            </Grid>
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}

export default GetPlaylists;
