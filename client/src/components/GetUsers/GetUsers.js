/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react';
import AuthContext from '../../providers/AuthContext.js';
import jwtDecode from "jwt-decode";
import { API_BASE_URL } from '../../common/constants.js';
import { Button, Table } from 'react-bootstrap';
import './GetUsers.css';

const GetUsers = () => {
    const [error, setError] = useState(null);
    const [allUsers, setAllUsers] = useState([]);
    const auth = useContext(AuthContext);

    const user_role = jwtDecode(localStorage.getItem('token')).role;
    const user_id = jwtDecode(localStorage.getItem('token')).id;

    const logout = () => {
        localStorage.removeItem('token');
        auth.setAuthState({
            user: null,
            isLoggedIn: false,
        })
    }
    useEffect(() => {
        getUsers();
    }, []);

    const getUsers = () => {
        fetch(`${API_BASE_URL}/admin/users`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
        })
        .then(response => {
            if (response.ok) {
                return response.json()
            }
            if (response.status === 401) {
                alert('Session expired!')
                logout()
            }
            throw new Error(response.statusText)
        })
        .then(result => {
            if (Array.isArray(result)) {
                setAllUsers(result);
            } else {
                throw new Error(result.message)
            }
        })
        .catch(error => {
            setError(error.message)
        })
    };

    const deleteUser = (id) => {
        fetch(`${API_BASE_URL}/admin/users/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then(() => getUsers());
    }

    const returnUser = (id) => {
        fetch(`${API_BASE_URL}/admin/users/${id}/return`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then(() => getUsers());
    }

    const makeAdmin = (id) => {
        fetch(`${API_BASE_URL}/admin/users/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
        })
        .then(() => getUsers());
    }

    const makeUser = (id) => {
        fetch(`${API_BASE_URL}/admin/users/${id}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
        })
        .then(() => getUsers());
    }

    // if (error) {
    //     return (
    //         <AppError message={error} />
    //     );
    // }

    return (
        <div className="all-users">
          <Table striped bordered hover size="sm">
            <thead>
              <tr className="users-title">
                <th>#</th>
                <th>Username</th>
                <th>Role</th>
                <th>Change role</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            {allUsers.map((u) => user_id !== u.id ?
            <tbody>
              <tr className="users-table">
                <td>{u.id}</td>
                <td>{u.username}</td>
                <td>{u.role === 1 ? 'User' : 'Admin'}</td>
                <td>{u.role === 1 ? 
                <div>
                <Button onClick={() => makeAdmin(u.id)}>Switch to admin ⛊</Button>
            </div>
            : <div>
            <Button onClick={() => makeUser(u.id)}>Switch to user ⛉</Button>
        </div>}</td>
                <td>{u.is_deleted === 0 ? 'Active' : 'Deleted'}</td>
                <td>{u.is_deleted === 0 ? 
                <div>
                <Button onClick={() => deleteUser(u.id)}>Delete user 🗑️</Button>
            </div>
            : <div>
            <Button onClick={() => returnUser(u.id)}>Return user ⏏️</Button>
        </div>}</td>
              </tr>
            </tbody>
            : '')}
          </Table>
        </div>
      );
};

export default GetUsers;

