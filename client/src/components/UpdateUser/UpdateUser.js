import { Button, InputLabel, TextField } from '@material-ui/core';
import { DropzoneDialog } from 'material-ui-dropzone';
import React, { Fragment, useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { API_BASE_URL, ROLES } from '../../common/constants.js';
import DEFALT_AVATAR from '../../common/index.png'
import AuthContext, { getUser } from '../../providers/AuthContext.js';
import './UpdateUser.css';

const UpdateUser = (props) => {

  const [open, setOpen] = React.useState(false);
  const [user, setUser] = useState({});
  const history = useHistory();

  useEffect(() => {
    fetch(`${API_BASE_URL}/users`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.message) {
          console.log(data.message);
        } else {
          if (data.avatar) {
            getAvatar(data);
          } else {
            setUser({
              id: data.id,
              username: data.username,
              role: data.role,
              avatar: data.avatar,
            });
          }
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  }, []);

  const getAvatar = (data) => {
    fetch(`${API_BASE_URL}/avatars/${data.avatar}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        Accept: 'image/png',
      },
    })
      .then((response) => response.blob())
      .then((avatar) => {
        console.log(data);
        setUser({
          id: data.id,
          username: data.username,
          role: data.role,
          avatar: data.avatar,
          avatarblob: avatar
        });
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const updateUsername = () => {
    fetch(`${API_BASE_URL}/users/username`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-type': 'application/json',
      },
      body: JSON.stringify({ username: user.username }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.message) {
          console.log(data.message);
        } else {
          console.log(data);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const setAvatar = (data) => {
    const file = user.avatarblob;
    const formData = new FormData();
    formData.append('image', file);
    formData.append('id', user.id);

    fetch(`${API_BASE_URL}/users/avatars`, {
      method: 'POST',
      body: formData,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        Accept: 'multipart/form-data',
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setUser({ ...user, avatar: data.avatar });
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  return (
    <div className="updateUser">
      <span className="avatar-wrap">
        {user.avatarblob ? (
          <img
            className='avatar-img'
            src={user.avatarblob ? URL.createObjectURL(user.avatarblob) : null}
            alt="err"
          />
        )
          :
          <img
            className='avatar-img'
            src={DEFALT_AVATAR}
            alt="err" />
        }
      </span>

      <span className='label'>
        <InputLabel display="inline">Insert new username:</InputLabel>
      </span>

      <span className='input'>
        <TextField id="standard-basic" value={user.username} onChange={e => setUser({ ...user, username: e.target.value })} />
      </span>

      <span className='label2'>
        <InputLabel>Role</InputLabel>
        <br />
        <InputLabel>{ROLES.USER === user.role ? 'user' : 'admin'}</InputLabel>
      </span>

      <span className='dropzone'>
        <Button variant="contained" size='small' color="primary" onClick={() => setOpen(true)}>
          Change Avatar
                    </Button>
        <DropzoneDialog
          acceptedFiles={['image/*']}
          cancelButtonText={"cancel"}
          submitButtonText={"submit"}
          maxFileSize={5000000}
          open={open}
          onClose={() => setOpen(false)}
          onSave={(files) => {
            setUser({ ...user, avatarblob: files[0] })
            setOpen(false);
          }}
          showPreviewsInDropzone={true}
          showPreviews={false}
        />
      </span>
      <span className='left-button'>
        <Button variant="contained" size='small' color="primary" onClick={() => { setAvatar(); updateUsername() }}>
          Save
            </Button>
        <Button variant="contained" size='small' color="primary" onClick={() => history.goBack()}>
          Back
            </Button>
      </span>
    </div>
  );
};

export default UpdateUser;
