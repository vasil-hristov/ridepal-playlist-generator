/* eslint-disable no-undef */
import { useEffect } from 'react';
import './Map.css';

const Map = () => {
    useEffect(()=>{
        const SetMapScript = document.createElement("script");
        const GetBingScript  = document.createElement("script");
        SetMapScript.type = GetBingScript.type = 'text/javascript';

        SetMapScript.innerText = (`
            function GetMap()
            {
                const map = new Microsoft.Maps.Map('#myMap', {});

                Microsoft.Maps.loadModule('Microsoft.Maps.Directions', () => {
                    const directionsManager = new Microsoft.Maps.Directions.DirectionsManager(map);
                    directionsManager.setRenderOptions({ itineraryContainer: '#directionsItinerary' });
                    directionsManager.showInputPanel('directionsPanel');
                });
            }
            `)
        GetBingScript.src = 'http://www.bing.com/api/maps/mapcontrol?callback=GetMap&key=AibL-mHgghKmW2HNyUldiX1iApN451Vc_nj5iub-Mtwx-GljtYtzt7zJzHCx_4ot'
        document.head.appendChild(SetMapScript);
        document.head.appendChild(GetBingScript);
    }, []);

return (
    <>
        <div className="directionsContainer">
            <div id="directionsPanel"></div>
            <div id="directionsItinerary"></div>
        </div>
        <div id="myMap"></div>

    </>
);
}
export default Map;
