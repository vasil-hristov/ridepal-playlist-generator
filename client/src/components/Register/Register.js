import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { API_BASE_URL } from "../../common/constants.js";
import { Button, TextField } from '@material-ui/core';
import './Register.css'

const Register = () => {
  const history = useHistory();
  const [form, setForm] = useState({
    username: {
      placeholder: "Insert username",
      value: "",
      type: "text",
      valid: true,
      validate: (value) => value.length >= 3,
      errorMessage: "least 3 symbols",
    },
    password: {
      placeholder: "Insert password",
      value: "",
      type: "password",
      valid: true,
      validate: (value) => value.length > 3,
      errorMessage: "least 3 symbols",
    },
    confirmPassword: {
      placeholder: "Confirm password",
      value: "",
      type: "password",
      valid: true,
      validate: (value) => value == form.password.value,
      errorMessage: "must mach the original",
    },
  });

  const [error, setError] = useState("");

  const register = (event) => {
    event.preventDefault();
    const isValid = Object.keys(form).every((key) =>
      form[key].validate(form[key].value)
    );

    if (isValid) {
      // build up object for the post request only with necessary data
      const data = Object.keys(form).reduce((acc, elementKey) => {
        return {
          ...acc,
          [elementKey]: form[elementKey].value,
        };
      }, {});

      fetch(`${API_BASE_URL}/users`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((r) => r.json())
        .then((r) => {
          if (r.error) {
            console.log("Greshka");
            throw new Error(r.message);
          }
          history.push("/users/login");
        })
        .catch((e) => setError(e.message));
    }
  };
  const handleInputChange = (event) => {
    const { name, value } = event.target;

    form[name].value = value;
    form[name].valid = form[name].validate(value);
    form[name].touched = true;

    const updatedForm = { ...form };
    setForm(updatedForm);
  };

  const formElements = Object.keys(form)
    .map((key) => {
      return {
        id: key,
        config: form[key],
      };
    })
    .map(({ id, config }) => {
      return (
        <div key={id}>
          <TextField
            id="standard-basic"
            label={config.placeholder}
            helperText={config.valid ? null : config.errorMessage}
            type={config.type}
            name={id}
            value={config.value}
            onChange={handleInputChange}
          />
        </div>
      );
    });

  // if (error) {
  //   return (
  //     <AppError message={error} />
  //   );
  // }

  return (
    <>
      <div className="register">
        <form onSubmit={register}>
          <h2>Register</h2>
          {formElements}
          <br />
          <Button type="submit" variant="outlined" size='small' color="primary">Sign up</Button>{'   '}
          <Button size='small' variant="outlined" onClick={() => history.push('/users/login')}>Log in</Button>
          <br />
          <br />
        </form>
      </div>
    </>
  );
};

export default Register;
