export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;
export const APP_BASE_URL = process.env.REACT_APP_APP_BASE_URL;

export const GENRES = {
    rock : 152,
    jazz : 129,
    pop : 132,
    classical : 98,
    blues : 153,
}

export const ROLES = {
    USER: 1,
    ADMIN: 2,
};
