/* eslint-disable no-self-assign */
export const formatDuration = (seconds) => {
    let hours = Math.floor(seconds / 3600);
    let mins = Math.floor((seconds % 3600) / 60);
    let secs = Math.floor(seconds % 60);
    
    mins < 10 ? mins = '0' + mins : mins = mins;
    secs < 10 ? secs = '0' + secs : secs = secs;

    return (hours ? `${hours}:${mins}:${secs} h` : `${mins}:${secs} mins`);
};
