import { Route, Redirect } from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';

const GuardedRoute = ({ component: Component, isLoggedIn, ...rest }) => {

  return (
    <Route {...rest} render={(props) => isLoggedIn ? <Component {...props} /> : <Redirect to='/home' /> } />
  )
};

GuardedRoute.propTypes = {
  component: PropTypes.func,
  isLoggedIn: PropTypes.object,
}

export default GuardedRoute;