import jwtDecode from "jwt-decode";
import { createContext } from "react";


const AuthContext = createContext({
    user: null,
    isLoggedIn: false,
    setAuthState: () => { },
});

export const getUser = () => {
    try {
        return jwtDecode(localStorage.getItem('token') || '');
    } catch (error) {
        return null;
    }
}

export default AuthContext;