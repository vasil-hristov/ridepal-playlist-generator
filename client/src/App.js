import './App.css';
import React, { useEffect, useState } from 'react'; 
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import AuthContext, { getUser } from './providers/AuthContext';
import jwtDecode from 'jwt-decode';
import Register from './components/Register/Register';
import Login from './components/Login/Login';
import Home from './components/Home/Home';
import Map from './components/Map/Map';
import GuardedRoute from './providers/GuardedRoute';
import GetUsers from './components/GetUsers/GetUsers';
import CreatePlaylist from './components/CreatePlaylist/CreatePlaylist';
import UpdateUser from './components/UpdateUser/UpdateUser.js';
import GetPlaylists from './components/GetPlaylists/GetPlaylists';
import Header from './components/Header/Header.js';
import Footer from './components/Footer/Footer.js';
import GetPlaylistById from './components/GetPlaylistById/GetPlaylistById';
import UpdatePlaylist from './components/UpdatePlaylist/UpdatePlaylist';

function App() {
  const [authValue, setAuthValue] = useState({
    user: getUser(),
    isLoggedIn: getUser(),
  })

  const logout = () => {
    localStorage.removeItem('token');

    setAuthValue({
      user: null,
      isLoggedIn: false,
    })
  }

  useEffect(() => {

    if (authValue.isLoggedIn) {
      const expiryDate = new Date(jwtDecode(localStorage.getItem('token')).exp * 1000)
      const currentDate = new Date();
      setTimeout(() => {
        alert('Session expired!')
        logout()

      }, expiryDate - currentDate)
    }

  }, [authValue.isLoggedIn])
  
  return (
    <div>
      <BrowserRouter>
        <AuthContext.Provider value={{ ...authValue, setAuthState: setAuthValue }}>
          <Header authValue={authValue} logout={logout}></Header>
          <Switch>
            <Redirect path="/" exact to="/home" />
            <Route path="/home" component={Home} />
            <Route path="/users" exact component={Register} />
            <Route path="/users/login" component={Login} />
            <Route path="/map" component={Map} />
            <Route path="/updateUser" component={UpdateUser} />
            <GuardedRoute path="/admin/users" isLoggedIn={authValue.isLoggedIn && authValue.user.role === 2} component={GetUsers} />
            <GuardedRoute path="/playlists" isLoggedIn={authValue.isLoggedIn} exact component={GetPlaylists} />
            <GuardedRoute path="/playlists/create" isLoggedIn={authValue.isLoggedIn} component={CreatePlaylist} />
            <GuardedRoute path="/playlists/:id" isLoggedIn={authValue.isLoggedIn} exact component={GetPlaylistById} />
            <GuardedRoute path="/playlists/:id/update" isLoggedIn={authValue.isLoggedIn} component={UpdatePlaylist} />
          </Switch>
          <Footer></Footer>
        </AuthContext.Provider>
      </BrowserRouter>
    </div>  
  );
}

export default App;
