import errors from '../common/errors.js';
import trackData from '../data/track-data.js';

const getById = async (id) => {
    const track = await trackData.getById(id);

    if (track.length < 1) {
        throw errors.NOT_FOUND;
    }

    return track;
};

const getWith = async (searchParams, user) => {
    if (!(Object.keys(searchParams).find(p => searchParams[p] !== undefined))) {
        return errors.BAD_REQUEST;
    }

    const response = await trackData.getWith(searchParams, user);

    if (response.length < 1) {
        return errors.NOT_FOUND;
    }
    return response;
};

export default {
    getById,
    getWith,
};
