import errors from '../common/errors.js';
import artistData from '../data/artist-data.js';

const getById = async (id) => {
    const artist = await artistData.getById(id);

    if (artist.length < 1) {
        throw errors.NOT_FOUND;
    }

    return artist;
};

export default {
    getById,
};
