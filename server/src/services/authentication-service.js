import userService from './user-service.js';
import bcrypt from 'bcrypt';
import tokenData from '../data/token-data.js'; 
import jwtService from './jwt-service.js';
import errors from '../common/errors.js';

let loggedUser = null;

const authenticate = async ({ username, password }) => {
  const user = await userService.getExistingUser(username);
  if (!await bcrypt.compare(password, user.password)) {
    throw errors.AUTHENTICATION_FAILED;
  }

  const token = jwtService.createToken({
    id: user.id,
    username: user.username,
    role: user.role,
    is_deleted: user.is_deleted,
  });

  await tokenData.create(token, user.id, user.username, user.role, user.is_deleted);
  return token;
};

export default {
  authenticate,
  loggedUser,
};
