import errors from '../common/errors.js';
import albumData from '../data/album-data.js';

const getById = async (id) => {
    const album = await albumData.getById(id);

    if (album.length < 1) {
        throw errors.NOT_FOUND;
    }

    return album;
};

export default {
    getById,
};