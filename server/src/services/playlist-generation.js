import trackData from '../data/track-data.js';

const playlistGeneration = async ({ title, duration, genres, repeatArtist }, userId) => {

    const playlist = {
        title,
        duration: 0,
        users_id: userId,
        tracks: '',
    };

    try {
        const tracks = await Promise.all(genres.map(async (g) => {
            return await Promise.allSettled(await trackData.getWith({ genres_id: g }));
        }));

        const artistsIncluded = [];
        while (playlist.duration < duration - 300) {
            const currentGenre = Math.floor(Math.random() * tracks.length);
            const currentTrack = tracks[currentGenre][Math.floor(Math.random() * tracks[currentGenre].length)];

            if (playlist.duration + currentTrack.duration > duration + 300) {
                continue;
            }
            if (!repeatArtist) {
                if (artistsIncluded.find(e => e === currentTrack.value.artists_id)) {
                    continue;
                }
                artistsIncluded.push(currentTrack.value.artists_id);
            }

            playlist.tracks += currentTrack.value.track_id + ' ';
            playlist.duration += currentTrack.value.duration;
        }
        return playlist;
    } catch (e) {
        return null;
    }
};

export default playlistGeneration;
