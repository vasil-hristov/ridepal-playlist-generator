import userData from '../data/user-data.js';
import bcrypt from 'bcrypt';
import errors from '../common/errors.js';

const create = async (userCreate) => {

  const { username, password } = userCreate;
  const existingUser = await userData.getBy('username', username);

  if (existingUser) {
    throw errors.CONFLICT;
  }

  const passwordHash = await bcrypt.hash(password, 10);
  const user = await userData.save(username, passwordHash);

  return user;
};

const getExistingUser = async (username) => {
  const user = await userData.getBy('username', username);

  if (!user) {
    throw errors.NOT_FOUND;
  }

  return user;
};

const getById = async (id) => {
  const user = await userData.getBy('id', id);

  if (!user) {
    throw errors.NOT_FOUND;
  }

  return user;
};

const updatePic = async ({ pic, id }) => {
  const response = await userData.updateAvatar(pic, id);
  
  return response;
};

const updateUsername = async (username, id) => {
  const response = await userData.updateUsername(username, id);

  return response;
};

export default {
  create,
  getExistingUser,
  updateUsername,
  getById,
  updatePic,
};
