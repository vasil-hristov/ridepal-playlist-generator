import tokenData from '../data/token-data.js';
import jwtService from './jwt-service.js';

const create = async (user) => {
  const token = jwtService.createToken({
    id: user.id,
    username: user.username,
  });

  await tokenData.create(token, user.id, user.username);
};

const remove = async (token) => {
  await tokenData.remove(token);
};

export default {
  create,
  remove,
};