
import errors from '../common/errors.js';
import playlistData from '../data/playlist-data.js';
import playlistGeneration from './playlist-generation.js';

const getById = async (id, user) => {
    const response = await playlistData.getById(id, user);
    if (response.length < 1) {
        return errors.NOT_FOUND;
    }
    return response;
};

const getWith = async (searchParams, user) => {
    const response = await playlistData.getWith(searchParams, user);
    if (response.length < 1) {
        return errors.NOT_FOUND;
    }
    return response;
};

const create = async (createParams, user) => {
    const playlist = await playlistGeneration(createParams, user.id);
    if (playlist === null) {
        return errors.SOMETHING_WENT_WRONG;
    }

    const mdResponse = await playlistData.create(playlist, user);

    return {...playlist, id: mdResponse.insertId};
};

const update = async (updatedPlaylist, playlistId, user) => {

    if (await getById(playlistId, user) === errors.NOT_FOUND) {
        return errors.NOT_FOUND;
    }

    const response = await playlistData.update(updatedPlaylist, playlistId, user);

    return response;
};

const remove = async (playlistId, user) => {
    if (await getById(playlistId, user) === errors.NOT_FOUND) {
        return errors.NOT_FOUND;
    }

    const response = await playlistData.remove(playlistId, user);

    return response;
};

const updatePic = async ({ pic, id }) => {
    const response = await playlistData.updateCover(pic, id);

    return response;
};

export default {
    getById,
    getWith,
    create,
    update,
    remove,
    updatePic,
};