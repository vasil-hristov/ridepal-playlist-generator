import adminData from '../data/admin-data.js';
import userData from '../data/user-data.js';
import errors from '../common/errors.js';

const getAll = async () => {
  return await userData.getAll();
};

const getById = async (id) => {
  const user = await userData.getBy('id', id);

  if (!user) {
    throw errors.NOT_FOUND;
  }

  return user;
};

const deleteUser = async (id) => {
  return await adminData.isDeleted(id);
};

const returnUser = async (id) => {
  return await adminData.notDeleted(id);
};

const promoteToAdmin = async (id) => {
  return await adminData.makeAdmin(id);
};

const promoteToUser = async (id) => {
  return await adminData.makeUser(id);
};

export default {
  getAll,
  deleteUser,
  returnUser,
  getById,
  promoteToAdmin,
  promoteToUser,
};
