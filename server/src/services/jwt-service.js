import jwt from 'jsonwebtoken';
import { PRIVATE_KEY } from '../config.js';

const createToken = (payload) => {
    const token = jwt.sign(
        payload,
        PRIVATE_KEY,
        { expiresIn: '1h' },
    );

    return token;
};

export default {
    createToken,
};
