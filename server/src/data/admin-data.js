import { ROLES } from '../common/constants.js';
import pool from './pool.js';


const isDeleted = async (id) => {
    const sql = `
    UPDATE users as u
    SET u.is_deleted = 1
    WHERE u.id = ?
    `;
  
    return await pool.query(sql, [id]);
};

const notDeleted = async (id) => {
    const sql = `
    UPDATE users as u
    SET u.is_deleted = 0
    WHERE u.id = ?
    `;
  
    return await pool.query(sql, [id]);
};

const makeAdmin = async (id) => {
  const sql = `
  UPDATE users as u
  SET u.role = ${ROLES.ADMIN}
  WHERE u.id = ?
  `;

  return await pool.query(sql, [id]); 
};

const makeUser = async (id) => {
  const sql = `
  UPDATE users as u
  SET u.role = ${ROLES.USER}
  WHERE u.id = ?
  `;

  return await pool.query(sql, [id]); 
};

export default {
    isDeleted,
    notDeleted,
    makeAdmin,
    makeUser,
};
