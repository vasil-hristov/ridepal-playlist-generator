import pool from './pool.js';

const getById = async (id) => {
    const sql = `
    SELECT *
    FROM tracks as t
    WHERE t.track_id = ?
    `;

    return await pool.query(sql, [id]);
};

const getWith = async (searchParams) => {
    const sql = `
    SELECT *
    FROM tracks
    ${Object.keys(searchParams).map(p=>
        searchParams[p] !== undefined 
            ? `WHERE ${p} = '${searchParams[p]}'`
            : '').join('')}
    `;

    return await pool.query(sql);
};

export default {
    getById,
    getWith,
};
