import pool from './pool.js';

const getById = async (id) => {
    const sql = `
    SELECT *
    FROM albums as al
    WHERE al.id = ?
    `;

    return await pool.query(sql, [id]);
};

  export default {
      getById,
  };