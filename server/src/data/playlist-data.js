import pool from './pool.js';
import { ROLES } from '../common/constants.js';

const getById = async (id, user) => {
  const sql = `
  SELECT *
  FROM playlists
  WHERE id = ?
  ${user.role === ROLES.USER ? 'AND is_deleted = 0' : ''}
  `;

  return await pool.query(sql, [id]);
};

const getWith = async (searchParams, user) => {
  const params = Object.keys(searchParams).find(e => searchParams[e] !== undefined);
  let sql = `
  SELECT *
  FROM playlists 
  ${params
    ?`WHERE ${Object.keys(searchParams).map(p => searchParams[p] !== undefined
      ? `${p} = '${searchParams[p]}' AND `
      : '').join('').slice(0, -5)}`
      : ''}
  ${user.role === ROLES.USER ? params ? 'AND is_deleted = 0' : 'WHERE is_deleted = 0' : ''}
  `;
  //sry it hurts my eyes either:/
  return await pool.query(sql);
};

const create = async (playlist) => {
  const sql = `
  INSERT INTO playlists
  SET ${Object.keys(playlist).map(p => `${p} = '${playlist[p]}'`).join(',')}`;

  return await pool.query(sql);
};

const update = async (playlist, id, user) => {
  const sql = `
  UPDATE playlists
  SET ${Object.keys(playlist).map(p => `${p} = '${playlist[p]}'`).join(',')}
  WHERE id = ?
  `;

  await pool.query(sql, [id]);

  return await getById(id, user);
};

const remove = async (id, user) => {
  const playlist = await getById(id, user);
  const sql = `
  UPDATE playlists
  SET is_deleted = 1
  WHERE id = ?
  `;

  await pool.query(sql, [id]);

  return playlist;
};

const updateCover = async ( pic, id ) => {
  const sql = `
    UPDATE playlists u
    SET u.picture = ?
    WHERE u.id = ?
  `;
  
  return await pool.query(sql, [pic, id]);
};


export default {
  getById,
  getWith,
  create,
  update,
  remove,
  updateCover,
};
