import pool from './pool.js';

const getById = async (id) => {
    const sql = `
    SELECT *
    FROM artists as a
    WHERE a.id = ?
    `;

    return await pool.query(sql, [id]);
};

  export default {
      getById,
  };
  