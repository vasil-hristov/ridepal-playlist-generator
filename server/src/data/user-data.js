/* eslint-disable no-prototype-builtins */
import pool from './pool.js';

const getAll = async () => {
  const sql = `
  SELECT u.id, u.username, u.role, u.is_deleted
  FROM users u
  `;

  return await pool.query(sql);
};

const getBy = async (column, value) => {
    const sql = `
    SELECT *
    FROM users
    WHERE ${column} = ?
    `;
    const result = await pool.query(sql, [value]);
  
    return result.hasOwnProperty('0') ? result[0] : null;
  };
  
  const save = async (username, password) => {
    const sql = `
      INSERT INTO users (username, password) 
      VALUES (?, ?)
    `;
  
    return await pool.query(sql, [username, password]);
  };

  const updateAvatar = async ( pic, id ) => {
    const sql = `
      UPDATE users u
      SET u.avatar = ?
      WHERE u.id = ?
    `;
    
    return await pool.query(sql, [pic, id]);
  };
  
  const updateUsername = async ( username, id ) => {
    const sql = `
      UPDATE users u
      SET u.username = ?
      WHERE u.id = ?
    `;

    return await pool.query(sql, [username, id]);
  };

export default {
    getAll,
    getBy,
    updateAvatar,
    updateUsername,
    save,
};
