/* eslint-disable no-prototype-builtins */
import pool from './pool.js';

const create = async (token) => {
  const sql = 'INSERT INTO tokens (token) VALUES (?)';

  return await pool.query(sql, [token]);
};

const findByToken = async (token) => {
  const sql = `
  SELECT token
  FROM tokens
  WHERE token = ?
  `;

  const result = await pool.query(sql, [token]);
  return result.hasOwnProperty('0') ? result[0] : null;
};

const remove = async (token) => {
  const sql = `
  DELETE FROM tokens 
  WHERE token = ?
  `;

  return await pool.query(sql, [token]);
};

export default {
  create,
  remove,
  findByToken,
};