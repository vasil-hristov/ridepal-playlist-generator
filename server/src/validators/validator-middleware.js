export const createValidator = schema => {
  return (req, res, next) => {
    const { body } = req;
    const validations = Object.keys(schema);

    const fails = validations
      .map(v => schema[v](body[v]))
      .filter(e => e !== null);

    if (fails.length > 0) {
      res.status(400).send(fails);
    } else {
      req.body = Object.keys(body)
        .filter(key => Object.keys(schema).includes(key))
        .reduce((obj, key) => {
          obj[key] = body[key];
          return obj;
        }, {});
      next();
    }
  };
};
