import { MAX_USERNAME_LENGTH, MIN_USERNAME_LENGTH,MAX_PASSWORD_LENGTH, MIN_PASSWORD_LENGTH } from '../../common/constants.js';

export const registerUserSchema = {
  username: value => {
    if (!value) {
      return 'Username is required';
    }

    if (typeof value !== 'string' || value.length < MIN_USERNAME_LENGTH || value.length > MAX_USERNAME_LENGTH) {
      return `Username should be a string in range [${MIN_USERNAME_LENGTH}...${MAX_USERNAME_LENGTH}]`;
    }
    return null;
  },
  password: value => {
    if (!value) {
      return 'Password is required';
    }

    if (typeof value !== 'string' || value.length < MIN_PASSWORD_LENGTH || value.length > MAX_PASSWORD_LENGTH) {
      return `Password should be a string in range [${MIN_PASSWORD_LENGTH}...${MAX_PASSWORD_LENGTH}]`;
    }

    return null;
  },
};
