import { MIN_PLAYLIST_TITLE_LENGTH, MAX_PLAYLIST_TITLE_LENGTH } from '../../common/constants.js';

export const updatePlaylistSchema = {
  title: value => {
    if (!value) {
      return 'Title is required';
    }

    if (typeof value !== 'string' || value.length < MIN_PLAYLIST_TITLE_LENGTH || value.length > MAX_PLAYLIST_TITLE_LENGTH) {
      return `Title should be a string in range [${MIN_PLAYLIST_TITLE_LENGTH}..${MAX_PLAYLIST_TITLE_LENGTH}]`;
    }

    return null;
  },
  // duration: value => {
  //   if (!value) {
  //     return 'Duration is required';
  //   }

  //   if (typeof value !== 'number' || value.length > 0 || value.length > MAX_PLAYLIST_DURATION) {
  //     return `Duration should be a string in range [0..${MAX_PLAYLIST_DURATION}]`;
  //   }

  //   return null;
  // },
  // tracks: value => {
  //   if (!value) {
  //     return 'Tracks is required';
  //   }

  //   if (typeof value !== 'string') {
  //     return `Tracks should be a string containing all tracks id's`;
  //   }

  //   return null;
  // },
};
