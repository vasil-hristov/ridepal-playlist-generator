import { MAX_PLAYLIST_DURATION, MIN_PLAYLIST_TITLE_LENGTH, MAX_PLAYLIST_TITLE_LENGTH, GENRES } from '../../common/constants.js';

export const createPlaylistSchema = {
  title: value => {
    if (!value) {
      return 'Title is required';
    }

    if (typeof value !== 'string' || value.length < MIN_PLAYLIST_TITLE_LENGTH || value.length > MAX_PLAYLIST_TITLE_LENGTH) {
      return `Title should be a string in range [${MIN_PLAYLIST_TITLE_LENGTH}..${MAX_PLAYLIST_TITLE_LENGTH}]`;
    }

    return null;
  },
  duration: value => {
    if (!value) {
      return 'Duration is required';
    }

    if (typeof value !== 'number' || value.length > 0 || value.length > MAX_PLAYLIST_DURATION) {
      return `Duration should be a number in range [0..${MAX_PLAYLIST_DURATION}]`;
    }

    return null;
  },
  genres: value => {
    if (!value) {
      return 'Genres is required';
    }

    if (!Array.isArray(value) || value.find((e)=> !GENRES.includes(e))) {
      return `Genre should be an array with possible values [${GENRES.join(', ').slice(0, -2)}]`;
    }

    return null;
  },
  repeatArtist: value => {
    if (value === undefined) {
      return 'repeatArtist is required';
    }

    if (typeof value !== 'number') {
      return 'repeatArtist should be a number in range [0 or 1]';
    }

    return null;
  },
};
