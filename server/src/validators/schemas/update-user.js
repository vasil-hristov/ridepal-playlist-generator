import { MAX_PASSWORD_LENGTH, MAX_STRING_LENGTH, MIN_PASSWORD_LENGTH, MIN_STRING_LENGTH } from '../../common/constants.js';

export const updateUserSchema = {
  username: value => {
    if (!value) {
      return 'username is required';
    }

    if (typeof value !== 'string' || value.length < MIN_STRING_LENGTH || value.length > MAX_STRING_LENGTH) {
      return `Name should be a string in range [${MIN_STRING_LENGTH}...${MAX_STRING_LENGTH}]`;
    }

    return null;
  },
  password: value => {
    if (!value) {
      return 'Password is required';
    }

    if (typeof value !== 'string' || value.length < MIN_PASSWORD_LENGTH || value.length > MAX_PASSWORD_LENGTH) {
      return `Password should be a string in range [${MIN_PASSWORD_LENGTH}...${MAX_PASSWORD_LENGTH}]`;
    }

    return null;
  },
};
