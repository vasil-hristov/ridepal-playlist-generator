import errors from '../common/errors.js';
import { ROLES } from '../common/constants.js';
import authenticationService from '../services/authentication-service.js';

const authorizationMiddleware = async (req, res, next) => {
  if (authenticationService.loggedUser.role !== ROLES.ADMIN) {
    return next(errors.FORBIDDEN);
  }
  
  await next();
};

export default authorizationMiddleware;
