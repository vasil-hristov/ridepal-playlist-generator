import errors from '../common/errors.js';
import tokenData from '../data/token-data.js';
import jwt from 'jsonwebtoken';
import { PRIVATE_KEY } from '../config.js';
import userService from '../services/user-service.js';
import authenticationService from '../services/authentication-service.js';

const authenticationMiddleware = async (req, res, next) => {
  const skipRoutes = ['/users', '/users/login'];

  if (skipRoutes.includes(req.path)) {
    return next();
  }

  const rawToken = req.header('authorization');

  if (!rawToken || rawToken.indexOf('Bearer ') !== 0) {
    return res.status(errors.UNAUTHORIZED.code).json(errors.UNAUTHORIZED);
  }

  const cleanToken = rawToken.replace('Bearer ', '');
  const searchToken = await tokenData.findByToken(cleanToken);

  if (!searchToken) {
    return res.status(errors.UNAUTHORIZED.code).json(errors.UNAUTHORIZED);
  }

  try {
    jwt.verify(cleanToken, PRIVATE_KEY);
  } catch (error) {
    return res.status(errors.AUTHENTICATION_FAILED.code).json(errors.AUTHENTICATION_FAILED);
  }
  try {
    const userToken = jwt.decode(searchToken.token);
    const user = await userService.getById(userToken.id);
    authenticationService.loggedUser = user;

  } catch (e) {
    return res.status(errors.AUTHENTICATION_FAILED.code).json(errors.AUTHENTICATION_FAILED);
  }

  await next();
};

export default authenticationMiddleware;
