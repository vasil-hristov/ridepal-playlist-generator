import express from 'express';
import errors from '../common/errors.js';
import authorizationMiddleware from '../middlewares/authorization.js';
import adminService from '../services/admin-service.js';

const adminController = express.Router();

adminController
  .get('/users', async (req, res) => {
    try {
      const users = await adminService.getAll(); 
      res.status(200).json(users);
    } catch (error) {
      res.json(errors.NOT_FOUND);
    }
  })
  .delete('/users/:id', authorizationMiddleware, async (req, res) => {
    try {
      const userId = +req.params.id;
      const user = await adminService.getById(userId);
  
      await adminService.deleteUser(userId);
  
      res.status(200).json(user);
    } catch (error) {
        res.json(errors.NOT_FOUND);
    }
  })
  .put('/users/:id/return', authorizationMiddleware, async (req, res) => {
    try {
      const userId = +req.params.id;
      const user = await adminService.getById(userId);
  
      await adminService.returnUser(userId);
  
      res.status(200).json(user);
    } catch (error) {
        res.json(errors.NOT_FOUND);
    }
  })
  .put('/users/:id', authorizationMiddleware, async (req, res) => {
    try {
      const userId = +req.params.id;
      const user = await adminService.getById(userId);

      await adminService.promoteToAdmin(userId); 

      res.status(200).json(user); 
    } catch (error) {
      res.json(errors.NOT_FOUND);
    }
  })
  .patch('/users/:id', authorizationMiddleware, async (req, res) => {
    try {
      const userId = +req.params.id;
      const user = await adminService.getById(userId);

      await adminService.promoteToUser(userId); 

      res.status(200).json(user); 
    } catch (error) {
      res.json(errors.NOT_FOUND);
    }
  });

  export default adminController;
