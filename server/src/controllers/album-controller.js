import express from 'express';
import errors from '../common/errors.js';
import albumService from '../services/album-service.js';

const albumController = express.Router(); 

albumController
.get('/:id', async (req, res) => {
    try {
        const id = +req.params.id;
        const album = await albumService.getById(id); 

        res.status(200).json(album);
    } catch (error) {
        res.json(errors.NOT_FOUND); 
    }

});

export default albumController;
