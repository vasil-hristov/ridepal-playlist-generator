import express from 'express';
import multer from 'multer';
import path from 'path';
import userService from '../services/user-service.js';
import authenticationService from '../services/authentication-service.js';
import tokenService from '../services/token-service.js';
import errors from '../common/errors.js';
import { createValidator } from '../validators/validator-middleware.js';
import { registerUserSchema } from '../validators/schemas/register-user.js';
import authenticationMiddleware from '../middlewares/authentication.js';

const userController = express.Router();

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'avatars');
  },
  filename(req, file, cb) {
    const filename = Date.now() + path.extname(file.originalname);

    cb(null, filename);
  },
});

const upload = multer({ storage });

userController
  .get('/', authenticationMiddleware, async (req, res) => {
    try {
      const response = await userService.getById(authenticationService.loggedUser.id);

      res.status(200).json(response);
    } catch (error) {
      res.status(errors.CONFLICT.code).json(errors.CONFLICT);
    }
  })
  .post('/', createValidator(registerUserSchema), async (req, res) => {
    const createData = req.body;

    try {
      await userService.create(createData);

      res.status(200).json({ message: `${req.body.username} registered` });
    } catch (error) {
      res.json(errors.CONFLICT);
    }
  })
  .put('/username', authenticationMiddleware, async (req, res) => {
    const username = req.body.username;

    try {
      await userService.updateUsername(username, authenticationService.loggedUser.id);

      res.status(200).json({ message: `${req.body.username} successfully renamed` });
    } catch (error) {
      res.json(error);
    }
  })
  .post('/avatars', upload.single('image'), async (req, res) => {
    if (!req.file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
      res.send({ msg: 'Only image files (jpg, jpeg, png) are allowed!' });
    } else {
      const image = req.file.filename;
      const { id } = req.body;
      try {
        await userService.updatePic({ pic: image, id });
        res.status(200).json('Upload succeeded');
      } catch (er) {
        console.log(er);
        res.status(400).json('Upload failed');
      }
    }
  })
  .post('/login', async (req, res) => {
    try {
      const token = await authenticationService.authenticate(req.body);

      res.status(200).json({ token });
    } catch (error) {
      res.status(errors.AUTHENTICATION_FAILED.code).json(errors.AUTHENTICATION_FAILED);
    }
  })
  .post('/logout', async (req, res) => {
    const token = req.headers.authorization.replace('Bearer ', '');

    await tokenService.remove(token);

    res.status(200).json({ message: 'User logged out successfully' });
  });

export default userController;