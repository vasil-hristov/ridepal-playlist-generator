import express from 'express';
import multer from 'multer';
import path from 'path';
import playlistService from '../services/playlist-service.js';
import errors from '../common/errors.js';
import { createValidator } from '../validators/validator-middleware.js';
import { createPlaylistSchema } from '../validators/schemas/create-playlist.js';
import { updatePlaylistSchema } from '../validators/schemas/update-playlist.js';
import authenticationService from '../services/authentication-service.js';

const playlistController = express.Router();

const storage = multer.diskStorage({
   destination(req, file, cb) {
      cb(null, 'covers');
   },
   filename(req, file, cb) {
      const filename = Date.now() + path.extname(file.originalname);

      cb(null, filename);
   },
});

const upload = multer({ storage });

playlistController
   .get('/:id', async (req, res) => {
      const { id } = req.params;
      const response = await playlistService.getById(id, authenticationService.loggedUser);

      if (response === errors.NOT_FOUND) {
         res.status(response.code).json(response);
      }

      res.status(200).json(response);
   })
   .get('/', async (req, res) => {
      const { title, duration, users_id } = req.query;
      console.log(title);
      const response = await playlistService.getWith({
         title: title,
         duration: duration,
         users_id: users_id,
      }, authenticationService.loggedUser);

      if (response === errors.BAD_REQUEST) {
         res.status(response.code).json(response);
      }

      res.status(200).json(response);
   })
   .post('/covers', upload.single('image'), async (req, res) => {
      if (!req.file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
        res.send({ msg: 'Only image files (jpg, jpeg, png) are allowed!' });
      } else {
        const image = req.file.filename;
        const { id } = req.body;
        try {
          await playlistService.updatePic({ pic: image, id });
          res.status(200).json('Upload succeeded');
        } catch (er) {
          console.log(er);
          res.status(400).json('Upload failed');
        }
      }
    })
   .post('/', createValidator(createPlaylistSchema), async (req, res) => {
      const { title, duration, genres, repeatArtist } = req.body;
      const response = await playlistService.create({ title, duration, genres, repeatArtist }, authenticationService.loggedUser);

      if (response === errors.SOMETHING_WENT_WRONG) {
         res.status(response.code).json(response);
      }

      res.status(200).json(response);
   })
   .put('/:id', createValidator(updatePlaylistSchema), async (req, res) => {
      const { title } = req.body;
      const playlistId = req.params.id;

      const response = await playlistService.update({ title }, +playlistId, authenticationService.loggedUser);

      if (response === errors.NOT_FOUND) {
         res.status(response.code).json(response);
      }

      res.status(200).json(response);

   })
   .delete('/:id', async (req, res) => {
      const playlistId = req.params.id;
      const response = await playlistService.remove(+playlistId, authenticationService.loggedUser);

      if (response === errors.NOT_FOUND) {
         res.status(response.code).json(response);
      }

      res.status(200).json(response);
   });

export default playlistController;