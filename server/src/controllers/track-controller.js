import express from 'express';
import errors from '../common/errors.js';
import trackService from '../services/track-service.js';

const trackController = express.Router(); 

trackController
.get('/:id', async (req, res) => {
    try {
        const id = +req.params.id;
        const track = await trackService.getById(id); 

        res.status(200).json(track);
    } catch (error) {
        res.json(errors.NOT_FOUND); 
    }

})
.get('/', async (req, res) => {
    const { title, duration, artists_id, genres_id, rank } = req.query;

    try {
        const response = await trackService.getWith({ title, duration, artists_id, genres_id, rank });
        
        res.status(200).json(response); 
    } catch (error) {
        res.json(errors.NOT_FOUND); 
    }
});

export default trackController;
