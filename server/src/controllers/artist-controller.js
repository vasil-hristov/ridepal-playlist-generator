import express from 'express';
import errors from '../common/errors.js';
import artistService from '../services/artist-service.js';

const artistController = express.Router(); 

artistController
.get('/:id', async (req, res) => {
    try {
        const id = +req.params.id;
        const artist = await artistService.getById(id);

        res.status(200).json(artist);
    } catch (error) {
        res.json(errors.NOT_FOUND); 
    }

});

export default artistController;
