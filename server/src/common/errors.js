export default {
    AUTHENTICATION_FAILED: {code: 401, message: 'Authentication failed'},
    NOT_FOUND: {code: 404, message: 'Not found'},
    CONFLICT: {code: 409, message: 'Conflict'},
    FORBIDDEN: {code: 403, message: 'Forbidden'},
    UNAUTHORIZED: {code: 401, message: 'Unauthorized'},
    BAD_REQUEST: {code: 400, message: 'Bad request'},
    SOMETHING_WENT_WRONG: {code: 500, message: 'Something went wrong'},
  };