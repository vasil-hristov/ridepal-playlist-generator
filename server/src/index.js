import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import dotenv from 'dotenv';
import userController from './controllers/user-controller.js';
import adminController from './controllers/admin-controller.js';
import playlistController from './controllers/playlist-controller.js';
import trackController from './controllers/track-controller.js';
import authenticationMiddleware from './middlewares/authentication.js';
import artistController from './controllers/artist-controller.js';
import albumController from './controllers/album-controller.js';

const config = dotenv.config().parsed;

const PORT = config.PORT;

const app = express();

app.use(cors());
app.use(helmet());
app.use(express.json());
app.use(authenticationMiddleware);
app.use('/users', userController); 
app.use('/admin', adminController);
app.use('/playlists', playlistController);
app.use('/tracks', trackController); 
app.use('/avatars', express.static('avatars'));
app.use('/covers', express.static('covers'));
app.use('/artists', artistController);
app.use('/albums', albumController);

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' }),
);

app.use((err, req, res) => {
  res.status(err.code).json(err);
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
