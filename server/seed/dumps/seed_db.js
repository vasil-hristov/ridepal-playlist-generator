import fetch from 'node-fetch';
import pool from '../../src/data/pool.js';
import { DEEZER_API_URL, ACCESS_TOKEN } from '../../src/common/constants.js';

const genresId = {
    rock : 152,
    jazz : 129,
    pop : 132,
    classical : 98,
    blues : 153,
};

const numbOfAlbumsToFetch = 300;

//function doesn't work with more than 899 albums to search 
const getTracksByAlbums = async (numbOfAlbums = 200) => {
    const genres = {};
    Object.keys(genresId).map(e=>genres[e] = []);
    console.log(genres);
    console.log('loading');
    for (let i = 0; i < numbOfAlbums; i++) {
        const response = await fetch(`${DEEZER_API_URL}album/302${999 - i}?${ACCESS_TOKEN}`).catch(console.log);
        const album = await response.json();
        
        const genre = Object.keys(genresId).find(e =>genresId[e] === album.genre_id);

        if (genre && genresId[genre] === album.genre_id) {
            genres[genre] = [...genres[genre], ...album.tracks.data.map(el=>{
                return {...el ,
                        albumId : '302' + (999 - i),
                        albumTitle : album.title,
                        albumCover : album.cover_medium,
                        genre: album.genre_id,
                    };
            })];
        }
    }
    return genres;
};


const seeddb = async (genres) => {
    Object.keys(genres).map(async genre=> {
    console.log('loading...');
    await Promise.all( genres[genre].map(async (track)=>{
        try{
            await pool.query('INSERT INTO ridepal.artists SET id = ? ,name = ?', [track.artist.id, track.artist.name]);
        }catch(e){
            console.log(e);
        }
    }));
  
    await Promise.all( genres[genre].map(async (track)=>{
        try{
            await pool.query('INSERT INTO ridepal.albums SET id = ? ,title = ?, cover_medium = ?, artists_id = ?, genres_id = ?', [track.albumId, track.albumTitle, track.albumCover, track.artist.id, track.genre]);
        }catch(e){
            console.log(e);
        }
    }));

    await Promise.all( genres[genre].map(async (track)=>{
        try{
            await pool.query('INSERT INTO ridepal.tracks SET id = ? ,title = ?, duration = ?, rank = ?, preview = ?, link = ?,albums_id = ?, artists_id = ?, genres_id = ?',
            [track.id, track.title, +track.duration, +track.rank, track.preview, track.link, track.albumId, track.artist.id, track.genre]);
        }catch(e){
            // console.log(e,track.genre);
        }
    }));
    });
    console.log('done');
};

const seed = async () => {
    console.log('start');
    await Promise.all(Object.keys(genresId).map(async genre => {
        try{
            await pool.query('INSERT INTO ridepal.genres SET id = ?, name = ?', [genresId[genre], genre]);
        }catch(e){
            console.log(e);
        }
    }));
    const genres = await getTracksByAlbums(numbOfAlbumsToFetch);
    seeddb(genres);
};

seed();